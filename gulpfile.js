var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var concat = require('gulp-concat');
var gutil = require( 'gulp-util' );
var ftp = require( 'vinyl-ftp' );
var autoprefixer = require('gulp-autoprefixer');
var rigger = require('gulp-rigger');
var imagemin = require('gulp-imagemin');
var connect = require('gulp-connect');
var sourcemaps = require('gulp-sourcemaps');
var cssbeautify = require('gulp-cssbeautify');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var spritesmith = require('gulp.spritesmith');

//-----------------------------------configuration----------------------------//
var hostAddress = 'hostAddress';
var userName = 'userName';
var userPass = 'userPass';
    var localFilesGlob = 'build/css/**';  
var localFilesGlobHtml = 'build/html/**';   
var localFilesGlobImage = 'build/images/**';  
var remoteFolder = '/home/divadubainew/public_html';
var remoteFolderHtml = '/home/divadubainew/public_html/html';
var remoteFolderImage = '/home/divadubainew/public_html/images';

//-----------------------------------path----------------------------//
var  path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: 'build/html',
        mobile: 'build/mobile',
        css: 'build/css',
        image: 'build/images'
    },
    src: { //Пути откуда брать исходники
        html: 'src/html/*.html',
        mobile: 'src/mobile/*.html',
        scss: 'src/scss/*.scss',
        image: 'src/images/*',
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/html/**',
        mobile: 'src/mobile/**',
        scss: 'src/scss/**',
    },
    clean: './build'
};
// server connect
gulp.task('connect', function() {
  connect.server({
    port: 8000,
    root: 'build',
    livereload: true
  });
});
// helper function to build an FTP connection based on our configuration
function getFtpConnection() {  
    return ftp.create({
        host: hostAddress,
        user: userName,
        password: userPass,
        parallel: 5,
        log: gutil.log
    });
}
//-----------------------------------image task----------------------------//
gulp.task('compress', function() {
  gulp.src(path.src.image)
  .pipe(imagemin())
  .pipe(gulp.dest(path.build.image))
});

//-----------------------------------html task----------------------------//
gulp.task('html', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(connect.reload())
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
});

//-----------------------------------mobile task----------------------------//
gulp.task('mobile', function () {
    gulp.src(path.src.mobile) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(connect.reload())
        .pipe(gulp.dest(path.build.mobile)) //Выплюнем их в папку build
});

//-----------------------------------SCSS to CSS----------------------------//
gulp.task('sass', function () {
  return gulp.src(path.src.scss)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['last 15 versions'],
        cascade: false
    }))
    .pipe(cssbeautify())
    .pipe(sourcemaps.init())
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(connect.reload())
    .pipe(gulp.dest(path.build.css));
});

gulp.task('sprite', function() {
    var spriteData = 
        gulp.src('src/images/sprite/*.*') // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.css',
                algorithm: 'binary-tree',
            }));

    spriteData.img.pipe(gulp.dest('build/images/sprite')); // путь, куда сохраняем картинку
    spriteData.css.pipe(gulp.dest('build/css/')); // путь, куда сохраняем стили
});

//-----------------------------------deploy task----------------------------//
// gulp.task('deploy', function() {
//     var conn = getFtpConnection();
//     return gulp.src(localFilesGlob, { base: 'build/', buffer: false })
//         .pipe( conn.newer( remoteFolder ) ) // only upload newer files 
//         .pipe( conn.dest( remoteFolder ) )
//     ;
// });
// gulp.task('deploy-html', function() {
//     var conn = getFtpConnection();
//     return gulp.src(localFilesGlobHtml, { base: 'build/', buffer: false })
//         .pipe( conn.newer( remoteFolder ) ) // only upload newer files 
//         .pipe( conn.dest( remoteFolder ) )
//     ;
// });
// gulp.task('deploy-image', function() {
//     var conn = getFtpConnection();
//     return gulp.src(localFilesGlobImage, { base: 'build/images/', buffer: false })
//         .pipe( conn.newer( remoteFolderImage ) ) // only upload newer files 
//         .pipe( conn.dest( remoteFolderImage ) )
//     ;
// });



//-----------------------------------watch task----------------------------//
gulp.task('sass:watch', function () {
  gulp.watch(path.watch.scss, ['sass']);
});
gulp.task('html:watch', function () {
  gulp.watch(path.watch.html, ['html']);
});
gulp.task('mobile:watch', function () {
  gulp.watch(path.watch.mobile, ['mobile']);
});
gulp.task('image:watch', function() {
  gulp.watch(path.src.image, ['compress']);
});
gulp.task('sprite:watch', function() {
  gulp.watch(path.src.image, ['sprite']);
});

//-----------------------------------ftp-deploy-watch----------------------------//
// gulp.task('ftp-deploy-watch', function() {
//     var conn = getFtpConnection();
//     gulp.watch(localFilesGlob)
//     .on('change', function(event) {
//     console.log('Changes detected! Uploading file "' + event.path + '", ' + event.type);
//     return gulp.src( [event.path], { base: 'build/', buffer: false } )
//     .pipe( conn.newer( remoteFolder ) ) // only upload newer files 
//     .pipe( conn.dest( remoteFolder ) )
//     ;
//     });
// });
// gulp.task('ftp-deploy-watch-html', function() {
//     var conn = getFtpConnection();
//     gulp.watch(localFilesGlobHtml)
//     .on('change', function(event) {
//     console.log('Changes detected! Uploading file "' + event.path + '", ' + event.type);
//     return gulp.src( [event.path], { base: 'build/html/', buffer: false } )
//     .pipe( conn.newer( remoteFolderHtml ) ) // only upload newer files 
//     .pipe( conn.dest( remoteFolderHtml ) )
//     ;
//     });
// });
// gulp.task('ftp-deploy-watch-image', function() {
//     var conn = getFtpConnection();
//     gulp.watch(localFilesGlobImage)
//     .on('change', function(event) {
//     console.log('Changes detected! Uploading file "' + event.path + '", ' + event.type);
//     return gulp.src( [event.path], { base: 'build/images/', buffer: false } )
//     .pipe( conn.newer( remoteFolderImage ) ) // only upload newer files 
//     .pipe( conn.dest( remoteFolderImage ) )
//     ;
//     });
// });

//-----------------------------------default task----------------------------//
// gulp.task('default', ['html', 'sass', 'ftp-deploy-watch', 'sass:watch', 'html:watch', 'ftp-deploy-watch-html', 'image:watch', 'ftp-deploy-watch-image']);
gulp.task('default', ['connect', 'html', 'sass', 'sass:watch', 'html:watch', 'compress', 'image:watch', 'sprite', 'sprite:watch', 'mobile:watch', 'mobile']);

