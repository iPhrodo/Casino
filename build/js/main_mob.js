$(document).ready(function(){
	var el = document.getElementById("body");
	var freezeVp = function(e) {
	    e.preventDefault();
	};
	$('.menuBtn__item').click(function(){
		$('.mobileNav').addClass('active');
		$('html').addClass('hidden');
	});
	$('.mobileNav__close').click(function(){
		$('.mobileNav').removeClass('active');
		$('html').removeClass('hidden');
	});
	$('.mobileNav__link_country').click(function(e){
		e.preventDefault();
		$('.countryBlock').addClass('active');
	});
	$('.countryBlock__close').click(function(){
		$('.countryBlock').removeClass('active');
	});
	$('select').customSelect({
		includeValue: true,
	});
	$('.custom-select__option:not(.custom-select__option--value)').livequery('click', function(){
		var selectText = $(this).parents('.custom-select').children('button').text();
		console.log(selectText)
		$(this).parents('.custom-select').find('.custom-select__option').removeClass('active');
		if (selectText === $(this).text()){
			$(this).addClass('active');
		}
	});
	$('.custom-select__option').livequery(function(){
		var selectText = $(this).parents('.custom-select').find('.custom-select__option--value').text();
		if (selectText === $(this).text()){
			$(this).addClass('active');
		}
	});
	$('.help__title:not(.active)').livequery('click',function(){
		$('.help__description').slideUp();
		$('.help__title').removeClass('active');
		$(this).toggleClass('active');
		$(this).next().slideToggle();
	});
	$('.help__title.active').livequery('click',function(){
		$(this).removeClass('active');
		$(this).next().slideUp();
	});
	$('.profile__item_1').click(function(e){
		e.preventDefault();
		$('.withdraw_popup').addClass('popup_profile_active');
	});
	$('.verify__btn_cont').click(function(e){
		e.preventDefault();
		$('.verify_popup').addClass('popup_profile_active');
	});
	$('.header__login').click(function(e){
		e.preventDefault();
		$('.popup__user_login').addClass('popup_profile_active');
		$('body, html').addClass('hidden');
		el.addEventListener("touchmove", freezeVp, false);
	});
	$('.register__link').click(function(e){
		e.preventDefault();
		$('.popup__user_register').addClass('popup_profile_active');
		$('body, html').addClass('hidden');
		el.addEventListener("touchmove", freezeVp, false);
	});
	$('.forgot__pass').click(function(e){
		e.preventDefault();
		$('.popup__user_forgot').addClass('popup_profile_active');
		$('body, html').addClass('hidden');
		el.addEventListener("touchmove", freezeVp, false);
	});
	$('.popup__close').click(function(){
		$('.popup_profile,.popup__user').removeClass('popup_profile_active');
		$('body, html').removeClass('hidden');
		el.removeEventListener("touchmove", freezeVp, false);
	});
	$('.form__select_help').livequery('change', function(){
		var option = $(this).val();
		console.log(option);
		$('.help__content').removeClass('help__content_active');
		$(this).parents('.help').find('.help__content.' + option).addClass('help__content_active');
	});
	$('.show_pass').click(function(){
		if ($(this).hasClass('active')){    		
			$(this).removeClass('active');
			$(this).parents('.form').find('input[name="password"]').prop('type', 'password');
		} else {
			$(this).addClass('active');
			$(this).parents('.form').find('input[name="password"]').prop('type', 'text');
		}
	});
	$('.all_pages .page_point').click(function(){
		$('.page_list').toggleClass('active');
	});
	$('.form__text').on('keyup', function(){
	    var val =  $(this).val();
	    if (val.length > 1){
	        $(this).addClass('not_empty');
	    }
	});
	$('textarea').on('keyup', function(){
	    var val =  $(this).val();
	    if (val.length > 1){
	        $(this).addClass('not_empty');
	    } else {
	        $(this).removeClass('not_empty');
	    }
	});
	$('.events__title').livequery('click', function(){
		$(this).toggleClass('events__title_active');
		$(this).next().slideToggle();
	});
	$('.sportTop__title').livequery('click', function(){
		$(this).toggleClass('events__title_active');
		$(this).parents('.sportTop__row').next().slideToggle();
	});
	$('.sport__slider').slick({
		arrows: false,
		dots: true
	});
	$('.horizontal_slider, .winner__slider, .lucky_slider').slick({
		arrows: false,
		dots: false,
		slidesToShow: 2,
		slidesToScroll: 1,
		responsive: [
			{
			  breakpoint: 767,
			  settings: {
			    slidesToShow: 1,
			    slidesToScroll: 1,
			  }
			}
		]
	});
	$('.games__more').click(function(){
		$('.explore_popup').addClass('active');
	});
	$('.explore_btn_3').livequery('click',function(){
		$('.explore_popup').removeClass('active');
	});
	setTimeout(function(){
		$('.liveStats__row:first-child .liveStats__part:nth-child(2)').livequery(function(){
			$(this).css({
				'width': '50%',
				'right': '0',
			});
		});
		$('.liveStats__row:nth-child(2) .liveStats__part:nth-child(2)').livequery(function(){
			$(this).css({
				'width': '60%',
				'right': '0',
			});
		});
		$('.liveStats__row:nth-child(3) .liveStats__part:nth-child(2)').livequery(function(){
			$(this).css({
				'width': '50%',
				'right': '0',
			});
		});
		$('.liveStats__row:nth-child(4) .liveStats__part:nth-child(1)').livequery(function(){
			$(this).css({
				'width': '80%',
				'left': '0',
			});
		});
		$('.liveStats__row:nth-child(5) .liveStats__part:nth-child(2)').livequery(function(){
			$(this).css({
				'width': '50%',
				'right': '0',
			});
		});
		$('.liveStats__row:nth-child(6) .liveStats__part:nth-child(2)').livequery(function(){
			$(this).css({
				'width': '75%',
				'right': '0',
			});
		});
		$('.liveStats__row:nth-child(7) .liveStats__part:nth-child(2)').livequery(function(){
			$(this).css({
				'width': '50%',
				'right': '0',
			});
		});
		$('.liveStats__row:nth-child(8) .liveStats__part:nth-child(1)').livequery(function(){
			$(this).css({
				'width': '60%',
				'left': '0',
			});
		});
	}, 500);

	$('.mobileSearch .form__text').livequery('focus', function(){
		$(this).parents('.mobileSearch').toggleClass('active');
		$(this).parents('.mobileSearch').append('<div class="close_search"></div>')
	});
	$('.close_search').livequery('click', function(){
		$(this).parents('.mobileSearch').removeClass('active');
		$('.mobileSearch .close_search').remove();
	});

	$('.events__tabs').on('touchstart',function(){
		$(this).addClass('active');
	});
	if ($(window).width() > 767){
		$('.winner__slider').insertAfter('.tablet_row2');
		$('.lucky_slider').insertAfter('.tablet_row3 .games__row:last-child');
		var cardH = $('.tablet_row1 .games__card_default').height();
		var cardH2 = $('.tablet_row1 .games__card_horizontal').height();
		var outH = cardH + cardH2 + 8;
		var imgH = $('.tablet_row1 .games__card_big .games__image').height();
		$('.tablet_row1 .games__card_big').height(outH);
		$('.tablet_row1 .games__card_big .games__body').height(outH - imgH);
		var cardH4 = $('.tablet_row4 .games__card_default').height();
		var outH4 = cardH4 + cardH4 + 8;
		var imgH4 = $('.tablet_row4 .games__card_big .games__image').height();
		$('.tablet_row4 .games__card_big').height(outH4);
		$('.tablet_row4 .games__card_big .games__body').height(outH4 - imgH4);
	}
	// $('.sport__nav').slick({
	// 	arrows: false,
	// 	slidesToShow: 4,
	// 	// slidesToScroll: 4,
	// 	responsive: [
	// 	    {
	// 	      breakpoint: 1200,
	// 	      settings: {
	// 	        slidesToShow: 12,
	// 	        // slidesToScroll: 6,
	// 	      }
	// 	    },
	// 	    {
	// 	      breakpoint: 991,
	// 	      settings: {
	// 	        slidesToShow: 9,
	// 	        // slidesToScroll: 4,
	// 	      }
	// 	    },
	// 	    {
	// 	      breakpoint: 767,
	// 	      settings: {
	// 	        slidesToShow: 6,
	// 	        // slidesToScroll: 4
	// 	      }
	// 	    },
	// 	    {
	// 	      breakpoint: 500,
	// 	      settings: {
	// 	        slidesToShow: 4,
	// 	        // slidesToScroll: 2
	// 	      }
	// 	    }
	// 	  ]
	// });

	$('.sportBlock__value').click(function(){
		$(this).toggleClass('sportBlock__value_active');
	});
	$('.events__item').click(function(){
		$(this).toggleClass('events__item_active');
	});

	$('.bettingSlip__icon-minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        $(this).parent().find('.bettingSlip__icon-plus').removeClass('active');
        $(this).addClass('active');
        return false;
    });
    $('.bettingSlip__icon-plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
         $(this).parent().find('.bettingSlip__icon-minus').removeClass('active');
        $(this).addClass('active');
        $input.change();
        return false;
    });
    $('.languageBlock__drop').livequery(function(){
    	var childCount = $(this).children().length;
    	console.log(childCount);
    	if (childCount < 2){
    		$(this).parents('.languageBlock').addClass('no_child');
    	}
    });
    $('.languageBlock__selected').livequery('click',function(){
    	$(this).parents('.languageBlock:not(.no_child)').find('.languageBlock__drop').toggleClass('active');
    });

    $('.languageBlock__item').click(function(){
    	var flag = $(this).html();
    	$(this).parents('.languageBlock').find('.languageBlock__body').remove();
    	$(this).parents('.languageBlock').find('.languageBlock__selected').prepend('<div class="languageBlock__body">'+flag+'</div>');
    	$(this).parents('.languageBlock').find('.languageBlock__drop').removeClass('active');
    });
    $('.nav_wrap').click(function(){
    	$(this).toggleClass('active');
    });
    $('.choice__label:not(.active)').click(function(){
    	$(this).next().addClass('active');
    	$(this).addClass('active');
    	$('.popup_transparent').fadeIn();
    });
    $('.choice__label.active, .popup_transparent').livequery('click',function(){
    	$('.choice__dropdown').removeClass('active');
    	$('.choice__label').removeClass('active');
    	$('.popup_transparent').fadeOut();
    });
    $('.choice__item').click(function(){
    	var img = $(this).html();
    	$('.choice__label').html(img);
    	$('.choice__dropdown').removeClass('active');
    	$('.choice__label').removeClass('active');
    	$('.popup_transparent').fadeOut();
    });

    $('.sportBlock .sportBlock__show:not(.sportBlock__show_active)').livequery('click', function(e){
    	$('.sportBlock .sportBlock__row_hide').slideDown();
    	$(this).addClass('sportBlock__show_active');
    	$(this).text('Show less');
    	e.preventDefault();
    });

    $('.sportBlock .sportBlock__show_active').livequery('click', function(e){
    	$('.sportBlock .sportBlock__row_hide').slideUp();
    	$(this).text('Show all');
    	$(this).removeClass('sportBlock__show_active');
    	e.preventDefault();
    });

    $('.sportTop .sportBlock__show:not(.sportBlock__show_active)').livequery('click', function(e){
    	$('.sportTop .sportTop__wrap_hide').slideDown();
    	$(this).addClass('sportBlock__show_active');
    	$(this).text('Show less');
    	e.preventDefault();
    });

    $('.sportTop .sportBlock__show_active').livequery('click', function(e){
    	$('.sportTop .sportTop__wrap_hide').slideUp();
    	$(this).text('Show all');
    	$(this).removeClass('sportBlock__show_active');
    	e.preventDefault();
    });
});