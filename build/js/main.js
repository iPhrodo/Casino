$(document).ready(function(){

	$(window).mousemove(function(e) {
	   var change;
	   var xpos=e.clientX;
	   var ypos=e.clientY;
       var positionX = xpos/20;
       var positionY = ypos/20;
	   $('.landing_main_img').css({
        'left': 'calc(50% - ' + positionX + 'px )',
        'top': 'calc(50% - ' + positionY + 'px )',
	   });        
       console.log(); 
	});

	$('.languageBlock__selected').livequery('click',function(){
		$(this).parents('.languageBlock:not(.no_child)').find('.languageBlock__drop').toggleClass('active');
	});

	$('.languageBlock__item').click(function(){
		var flag = $(this).html();
		$(this).parents('.languageBlock').find('.languageBlock__body').remove();
		$(this).parents('.languageBlock').find('.languageBlock__selected').prepend('<div class="languageBlock__body">'+flag+'</div>');
		$(this).parents('.languageBlock').find('.languageBlock__drop').removeClass('active');
	});

	$('.slider').slick({
		dots: true,
	});
	$('.sportsSlider__content').slick({
		dots: true,
		fade: true
	});
 
	$('.slider .slick-dots').wrap('<div class="dots_wrap"></div>');
	$('.slider .dots_wrap').prepend('<div class="slick__arrow slick__prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>');
	$('.slider .dots_wrap').append('<div class="slick__arrow slick__next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>');
	
	$('.slick__prev').livequery('mousedown', function(){
		$('.slider .slick-prev').click();
	});
	$('.slick__next').livequery('mousedown', function(){
		$('.slider .slick-next').click();
	});

	$('.sportsSlider__content .slick-dots').wrap('<div class="dots_wrap"></div>');
	$('.sportsSlider__content .dots_wrap').prepend('<div class="slick__arrow slick__prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>');
	$('.sportsSlider__content .dots_wrap').append('<div class="slick__arrow slick__next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>');
	$('.slick__prev').mousedown(function(){
		$('.sportsSlider__content .slick-prev').click();
	});

	$('.slick__next').mousedown(function(){
		$('.sportsSlider__content .slick-next').click();
	});

	$('.loveBlock__item').click(function(){
		var tid = $(this).data('tab');
		$('.loveBlock__item').removeClass('active');
		$('.tab').removeClass('active');
		$(this).addClass('active');
		$('.tab.' + tid).addClass('active');
	});

	$('select').customSelect({
		includeValue: true,
	});
	$('.custom-select__option:not(.custom-select__option--value)').livequery('click', function(){
		var selectText = $(this).parents('.custom-select').find('.custom-select__option--value').text();
		$(this).parents('.custom-select').find('.custom-select__option').removeClass('active');
		if (selectText === $(this).text()){
			$(this).addClass('active');
		}
	});
	$('.custom-select__option').livequery(function(){
		var selectText = $(this).parents('.custom-select').find('.custom-select__option--value').text();
		if (selectText === $(this).text()){
			$(this).addClass('active');
		}
	});
	// $('.custom-select__dropdown').livequery(function(){
	// 	$(this).mCustomScrollbar();
	// });
	// $('.custom-select__dropdown').mCustomScrollbar();
	$('.userLogin__nav').click(function(){
		$('.header__info').append('<div class="popup_transparent"></div>');
		$('.header__info').find('.popup_transparent').fadeIn();
		$('.userLogin__dropdown').addClass('userLogin__dropdown_active');
	});
	$(document).on('click', '.popup_transparent',function(){
		$('.header__info').find('.popup_transparent').remove();
		$('.userLogin__dropdown').removeClass('userLogin__dropdown_active');
	});
	$('.faqBlock__title').click(function(){
		$(this).toggleClass('active');
		$(this).parents('.faqBlock__row').toggleClass('faqBlock__row_active');
		$(this).next().slideToggle();
	});
	$('.terms__link').click(function(e){
		e.preventDefault();
		$(this).toggleClass('active');
		// $(this).parents('.faqBlock__row').toggleClass('faqBlock__row_active');
		$(this).next().slideToggle();
	});
	$('#file').change(function(){
		var filename = $(this).val().replace(/.*\\/, "");
		$('.form__file_text').text(filename);
	});
	$('.account__btn_phone').click(function(e){
		e.preventDefault();
		$('.popups__user_phone, .popup').fadeIn();
	});
	$('.account__btn_mail').click(function(e){
		e.preventDefault();
		$('.popups__user_mail, .popup').fadeIn();
	});
	$('.settings__edit_phone').click(function(e){
		e.preventDefault();
		$('.popups__user_change-phone, .popup').fadeIn();
	});
	$('.settings__edit_email').click(function(e){
		e.preventDefault();
		$('.popups__user_change-email, .popup').fadeIn();
	});
	$('.settings__btn_verify').click(function(e){
		e.preventDefault();
		$('.popups__user_verify, .popup').fadeIn();
	});
	$('.settings__btn_pass').click(function(e){
		e.preventDefault();
		$('.popups__user_verify-not, .popup').fadeIn();
	});
	$('.settings__edit_details').click(function(e){
		e.preventDefault();
		$('.popups__user_details, .popup').fadeIn();
	});

	$('.popups__close').click(function(){
		$('.popups__user, .popup').fadeOut();
	});

	// PASSWORD STRENGTH
	$('.form_password #new_pass').strengthMeter('text', {
	    container: $('.form__security'),
	    hierarchy: {
	        '1': ['form__security', 'Weak'],
	        '20': ['form__security', 'So-so'],
	        '30': ['form__security', 'Strong']
	    }
	});
    $(document).on('keyup', '.form_password #new_pass', function(){
    	var textPass = $(this).parents('.form_password').find('.form__security').text();
		 if (textPass == 'Weak'){
			$(this).parents('.form_password').find('.form__security').addClass('weak').removeClass('so_so').removeClass('strong').append('<div class="form__security_item form__security_item_1"><div class="form__security_content"></div></div><div class="form__security_item form__security_item_2"><div class="form__security_content"></div></div><div class="form__security_item form__security_item_3"><div class="form__security_content"></div></div> ');
    	} else if (textPass == 'So-so'){
			$(this).parents('.form_password').find('.form__security').addClass('so_so').removeClass('weak').removeClass('strong').append('<div class="form__security_item form__security_item_1"><div class="form__security_content"></div></div><div class="form__security_item form__security_item_2"><div class="form__security_content"></div></div><div class="form__security_item form__security_item_3"><div class="form__security_content"></div></div> ');    		
    	} else if (textPass == 'Strong'){
			$(this).parents('.form_password').find('.form__security').addClass('strong').removeClass('weak').removeClass('so_so').append('<div class="form__security_item form__security_item_1"><div class="form__security_content"></div></div><div class="form__security_item form__security_item_2"><div class="form__security_content"></div></div><div class="form__security_item form__security_item_3"><div class="form__security_content"></div></div> ');    		
    	} else {
    		$(this).parents('.form_password').find('.form__security').removeClass('strong').removeClass('weak').removeClass('so_so')
    	}
    });
    $('.form__text').on('keyup', function(){
        var val =  $(this).val();
        if (val.length > 1){
            $(this).addClass('not_empty');
        } else {
            $(this).removeClass('not_empty');
        }
    });
    $('textarea').on('keyup', function(){
        var val =  $(this).val();
        if (val.length > 1){
            $(this).addClass('not_empty');
        } else {
            $(this).removeClass('not_empty');
        }
    });
    $('.choice__label:not(.active)').click(function(){
    	$(this).next().addClass('active');
    	$(this).addClass('active');
    	$('.popup_transparent').fadeIn();
    });
    $('.choice__label.active, .popup_transparent').livequery('click',function(){
    	$('.choice__dropdown').removeClass('active');
    	$('.choice__label').removeClass('active');
    	$('.popup_transparent').fadeOut();
    });
    $('.choice__item').click(function(){
    	var img = $(this).html();
    	$('.choice__label').html(img);
    	$('.choice__dropdown').removeClass('active');
    	$('.choice__label').removeClass('active');
    	$('.popup_transparent').fadeOut();
    });

    if ($('.wrapper').hasClass('page_landing_main') && $(window).width() < 1590 ){
    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.landing__nav').height() + $('.footer').height();
    	var innerHeight = $(window).height() - allHeight;
        if (innerHeight < 370){
            innerHeight = 370;
        }
    	$('.landing__center').height(innerHeight - 53);
    	$(window).resize(function(){
	    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.landing__nav').height() + $('.footer').height();
	    	var innerHeight = $(window).height() - allHeight;
            if (innerHeight < 370){
                innerHeight = 370;
            }
	    	$('.landing__center').height(innerHeight - 53);
    	});
    }
    if ($('.wrapper').hasClass('page_landing_main') && $(window).width() < 1590 && $(window).height() > 700 ){
    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.landing__nav').height() + $('.footer').height();
    	var innerHeight = $(window).height() - allHeight;
        if (innerHeight < 420){
            innerHeight = 420;
        }
    	$('.landing__center').height(innerHeight - 100);
    	$(window).resize(function(){
	    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.landing__nav').height() + $('.footer').height();
	    	var innerHeight = $(window).height() - allHeight;
            if (innerHeight < 420){
                innerHeight = 420;
            }
	    	$('.landing__center').height(innerHeight - 100);
    	});
    }

    if ($('.wrapper').hasClass('page_landing_main') && $(window).width() > 1591 && $(window).width() < 1850 ){
    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.landing__nav').height() + $('.footer').height();
    	var innerHeight = $(window).height() - allHeight;
        if (innerHeight < 500){
            innerHeight = 500;
        }
    	$('.landing__center').height(innerHeight - 175);
    	$(window).resize(function(){
	    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.landing__nav').height() + $('.footer').height();
	    	var innerHeight = $(window).height() - allHeight;
            if (innerHeight < 500){
                innerHeight = 500;
            }
	    	$('.landing__center').height(innerHeight - 175);
    	});
    }
    if ($('.wrapper').hasClass('page_landing_main') && $(window).width() > 1850 ){
    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.landing__nav').height() + $('.footer').height();
    	var innerHeight = $(window).height() - allHeight;
        if (innerHeight < 500){
            innerHeight = 500;
        }
    	$('.landing__center').height(innerHeight - 150);
    	$(window).resize(function(){
	    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.landing__nav').height() + $('.footer').height();
	    	var innerHeight = $(window).height() - allHeight;
            if (innerHeight < 500){
                innerHeight = 500;
            }
	    	$('.landing__center').height(innerHeight - 150);
    	});
    }

    if ($('.wrapper').hasClass('page_landing_deposit') && $(window).width() < 1740 ){
    	var allHeight = 26 + $('.deposit__logo').height();
    	var innerHeight = $(window).height() - allHeight;
        if (innerHeight < 400){
            innerHeight = 400;
        }
    	$('.landing_deposit').height(innerHeight - 75);
    	$(window).resize(function(){
	    	var allHeight = 26 + $('.deposit__logo').height();
	    	var innerHeight = $(window).height() - allHeight;
            if (innerHeight < 400){
                innerHeight = 400;
            }
	    	$('.landing_deposit').height(innerHeight - 75);
    	});
    }

    if ($('.wrapper').hasClass('page_landing_login') && $(window).width() < 1740 ){
    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.footer').height();
    	var innerHeight = $(window).height() - allHeight;
        if (innerHeight < 400){
            innerHeight = 400;
        }
    	$('.landing_login').height(innerHeight - 75);
    	$(window).resize(function(){
	    	var allHeight = 26 + $('.deposit__logo_with').height() + $('.footer').height();
	    	var innerHeight = $(window).height() - allHeight;
            if (innerHeight < 400){
                innerHeight = 400;
            }
	    	$('.landing_login').height(innerHeight - 75);
    	});
    }


    if ($('.wrapper').hasClass('page_game') && $(window).width() < 1940 && $(window).width() > 1541){
		var windowWidth = $(window).width();
    	var blockWidth = $('.winners_game').width() + $('.gameDeposit').width() + 50;
    	var gameWidth = windowWidth - blockWidth;
    	$('.game').width(gameWidth);
	    $(window).resize(function(){
	    	if ($(window).width() < 1940 && $(window).width() > 1541){
	    		var windowWidth = $(window).width();
		    	var blockWidth = $('.winners_game').width() + $('.gameDeposit').width() + 50;
		    	var gameWidth = windowWidth - blockWidth;
		    	$('.game').width(gameWidth);
	    	}
	    });
    }
    if ($(window).width() < 1540){
        $('.winners_game').removeClass('winners_game_active');
        $('.gameDeposit').removeClass('gameDeposit_active');
        $('.gameDeposit__btn').addClass('active');
        $('.winners_game__btn').addClass('active');
    }
    $(window).resize(function(){
        if ($(window).width() < 1540){
    	    $('.winners_game').removeClass('winners_game_active');
    	    $('.gameDeposit').removeClass('gameDeposit_active');
    	    $('.gameDeposit__btn').addClass('active');
    	    $('.winners_game__btn').addClass('active');
        }
    });

    $('.gameDeposit__btn').livequery('click', function(){
    	$('.gameDeposit').addClass('gameDeposit_active');	
    	$(this).removeClass('active');
		var windowWidth = $(window).width();
    	var blockWidth = $('.winners_game').width() + $('.gameDeposit').width() + 50;
    	var gameWidth = windowWidth - blockWidth;
    	// $('.game').width(gameWidth);
    	console.log(gameWidth);
    });
    $('.winners_game__btn').livequery('click', function(){
    	$('.winners_game').addClass('winners_game_active');	
    	$(this).removeClass('active');
		var windowWidth = $(window).width();
    	var blockWidth = $('.winners_game').width() + $('.gameDeposit').width() + 50;
    	var gameWidth = windowWidth - blockWidth;
    	// $('.game').width(gameWidth);
    	console.log(gameWidth);
    });

    $('.gameDeposit__close').livequery('click', function(){
    	$('.gameDeposit').removeClass('gameDeposit_active');
    	$('.gameDeposit__btn').addClass('active');
    	// $('.game').width('100%');
	});
	$('.winners__close').livequery('click', function(){
    	$('.winners_game').removeClass('winners_game_active');
    	$('.winners_game__btn').addClass('active');
    	// $('.game').width('100%');
	});

    $('.allSports__link_parent .fa').livequery('click', function(e){
    	e.preventDefault();
    	$(this).toggleClass('active');
    	$(this).parents('.allSports__item').find('.allSports__dropdown').slideToggle();
    });

    $('.sports__first').click(function(){
    	$(this).toggleClass('active');
    });
    $('.sports__second').click(function(){
    	$(this).toggleClass('active');
    });

    $('.close__label').click(function(){
    	$(this).parents('.betSlip__row').remove();
    });
    $('.betSlip__clear').click(function(){
    	$(this).parents('.betSlip__form').find('.betSlip__row').remove();
    });
    $('.drop__title').livequery('click',function(){
    	$(this).next().slideToggle();
    	if ($(this).hasClass('drop__title_active')){

    	} else{
    		$(this).toggleClass('drop__title_active');
    	}
    });
    $('.drop__item').livequery('click',function(){
    	var dropItem =  $(this).text();
    	$('.drop__item').removeClass('drop__item_active');
    	$(this).addClass('drop__item_active');
    	$(this).parents('.drop').find('.drop__title').text(dropItem);
    	$(this).parents('.drop').find('.drop__dropdown').slideUp();
    	$(this).parents('.drop').find('.drop__title').addClass('drop__title_active');
    });
    $('.drop__item').livequery(function(){
    	var titleText = $(this).parents('.drop').find('.drop__title').text();
    	if (titleText === $(this).text()){
    		$(this).addClass('drop__item_active');
    	}
    });

    $('.tournament__title').click(function(){
    	$(this).toggleClass('tournament__title_active');
    	$(this).next().toggleClass('tournament__content_active');
    });
    $('.liveBlock__name').click(function(){
    	$(this).toggleClass('liveBlock__name_active');
    	$(this).next().toggleClass('tournament_active');
    });
    $('.eventBlock__btn').click(function(){
    	$(this).toggleClass('eventBlock__btn_active');
    	$(this).parents('.eventBlock__header').next().slideToggle();
    });
    $('.live__coeff').click(function(){
    	$(this).toggleClass('live__coeff_active');
    });

    $('.games__more').click(function(){
    	$('.explore_popup').fadeIn();
    });
    $('.languageBlock__drop').livequery(function(){
    	var childCount = $(this).children().length;
    	console.log(childCount);
    	if (childCount < 2){
    		$(this).parents('.languageBlock').addClass('no_child');
    	}
    });

    $('.landingSteps__title_code').click(function(){
        $('.deposit__item_hide').slideToggle();
    });

});